// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Private/VictoryUMGPrivatePCH.h"
#include "Public/JoyColorWheel.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeJoyColorWheel() {}
// Cross Module References
	VICTORYUMG_API UFunction* Z_Construct_UDelegateFunction_VictoryUMG_OnJoyColorChangedEvent__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_VictoryUMG();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	VICTORYUMG_API UFunction* Z_Construct_UFunction_UJoyColorWheel_GetColor();
	VICTORYUMG_API UClass* Z_Construct_UClass_UJoyColorWheel();
	VICTORYUMG_API UFunction* Z_Construct_UFunction_UJoyColorWheel_SetColor();
	VICTORYUMG_API UClass* Z_Construct_UClass_UJoyColorWheel_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UWidget();
// End Cross Module References
	UFunction* Z_Construct_UDelegateFunction_VictoryUMG_OnJoyColorChangedEvent__DelegateSignature()
	{
		struct _Script_VictoryUMG_eventOnJoyColorChangedEvent_Parms
		{
			FLinearColor NewColor;
		};
		UObject* Outer = Z_Construct_UPackage__Script_VictoryUMG();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnJoyColorChangedEvent__DelegateSignature"), RF_Public|RF_Transient|RF_MarkAsNative) UDelegateFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x00130000, 65535, sizeof(_Script_VictoryUMG_eventOnJoyColorChangedEvent_Parms));
			UProperty* NewProp_NewColor = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("NewColor"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(NewColor, _Script_VictoryUMG_eventOnJoyColorChangedEvent_Parms), 0x0010000008000182, Z_Construct_UScriptStruct_FLinearColor());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/JoyColorWheel.h"));
			MetaData->SetValue(NewProp_NewColor, TEXT("NativeConst"), TEXT(""));
#endif
		}
		return ReturnFunction;
	}
	void UJoyColorWheel::StaticRegisterNativesUJoyColorWheel()
	{
		UClass* Class = UJoyColorWheel::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "GetColor", (Native)&UJoyColorWheel::execGetColor },
			{ "SetColor", (Native)&UJoyColorWheel::execSetColor },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, ARRAY_COUNT(AnsiFuncs));
	}
	UFunction* Z_Construct_UFunction_UJoyColorWheel_GetColor()
	{
		struct JoyColorWheel_eventGetColor_Parms
		{
			FLinearColor ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_UJoyColorWheel();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetColor"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x14820401, 65535, sizeof(JoyColorWheel_eventGetColor_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(ReturnValue, JoyColorWheel_eventGetColor_Parms), 0x0010000000000580, Z_Construct_UScriptStruct_FLinearColor());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Joy Color Wheel"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/JoyColorWheel.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Get Color!"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UJoyColorWheel_SetColor()
	{
		struct JoyColorWheel_eventSetColor_Parms
		{
			FLinearColor NewColor;
			bool SkipAnimation;
		};
		UObject* Outer = Z_Construct_UClass_UJoyColorWheel();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SetColor"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04820401, 65535, sizeof(JoyColorWheel_eventSetColor_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(SkipAnimation, JoyColorWheel_eventSetColor_Parms);
			UProperty* NewProp_SkipAnimation = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("SkipAnimation"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(SkipAnimation, JoyColorWheel_eventSetColor_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(SkipAnimation, JoyColorWheel_eventSetColor_Parms), sizeof(bool), true);
			UProperty* NewProp_NewColor = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("NewColor"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(NewColor, JoyColorWheel_eventSetColor_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FLinearColor());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Joy Color Wheel"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_SkipAnimation"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/JoyColorWheel.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Set Color Picker's Color!"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UJoyColorWheel_NoRegister()
	{
		return UJoyColorWheel::StaticClass();
	}
	UClass* Z_Construct_UClass_UJoyColorWheel()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UWidget();
			Z_Construct_UPackage__Script_VictoryUMG();
			OuterClass = UJoyColorWheel::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20B00080u;

				OuterClass->LinkChild(Z_Construct_UFunction_UJoyColorWheel_GetColor());
				OuterClass->LinkChild(Z_Construct_UFunction_UJoyColorWheel_SetColor());

				UProperty* NewProp_OnColorChanged = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("OnColorChanged"), RF_Public|RF_Transient|RF_MarkAsNative) UMulticastDelegateProperty(CPP_PROPERTY_BASE(OnColorChanged, UJoyColorWheel), 0x0010000010080000, Z_Construct_UDelegateFunction_VictoryUMG_OnJoyColorChangedEvent__DelegateSignature());
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bSkipAnimationOnConstruction, UJoyColorWheel);
				UProperty* NewProp_bSkipAnimationOnConstruction = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bSkipAnimationOnConstruction"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bSkipAnimationOnConstruction, UJoyColorWheel), 0x0010000000010015, CPP_BOOL_PROPERTY_BITMASK(bSkipAnimationOnConstruction, UJoyColorWheel), sizeof(bool), true);
				UProperty* NewProp_JoyColor = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("JoyColor"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(JoyColor, UJoyColorWheel), 0x0010000000010015, Z_Construct_UScriptStruct_FLinearColor());
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UJoyColorWheel_GetColor(), "GetColor"); // 3435799446
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UJoyColorWheel_SetColor(), "SetColor"); // 1480747525
				static TCppClassTypeInfo<TCppClassTypeTraits<UJoyColorWheel> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("JoyColorWheel.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/JoyColorWheel.h"));
				MetaData->SetValue(NewProp_OnColorChanged, TEXT("Category"), TEXT("Widget Event"));
				MetaData->SetValue(NewProp_OnColorChanged, TEXT("DisplayName"), TEXT("OnColorChanged (Joy Color Wheel)"));
				MetaData->SetValue(NewProp_OnColorChanged, TEXT("ModuleRelativePath"), TEXT("Public/JoyColorWheel.h"));
				MetaData->SetValue(NewProp_OnColorChanged, TEXT("ToolTip"), TEXT("Called whenever the color is changed! Yay!"));
				MetaData->SetValue(NewProp_bSkipAnimationOnConstruction, TEXT("Category"), TEXT("Joy Color Wheel"));
				MetaData->SetValue(NewProp_bSkipAnimationOnConstruction, TEXT("ModuleRelativePath"), TEXT("Public/JoyColorWheel.h"));
				MetaData->SetValue(NewProp_bSkipAnimationOnConstruction, TEXT("ToolTip"), TEXT("Should the color picker jump instantly to the chosen JoyColor when it is first created?"));
				MetaData->SetValue(NewProp_JoyColor, TEXT("Category"), TEXT("Joy Color Wheel"));
				MetaData->SetValue(NewProp_JoyColor, TEXT("ModuleRelativePath"), TEXT("Public/JoyColorWheel.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UJoyColorWheel, 3313551197);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UJoyColorWheel(Z_Construct_UClass_UJoyColorWheel, &UJoyColorWheel::StaticClass, TEXT("/Script/VictoryUMG"), TEXT("UJoyColorWheel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UJoyColorWheel);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
